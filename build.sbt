lazy val root = (project in file(".")).
  settings(
    name := "mbed UDP client test script",
    version := "0.20.18",
    scalaVersion := "2.12.5",
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-language:existentials")
  )
