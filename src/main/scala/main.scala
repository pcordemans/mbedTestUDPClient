import scala.io.StdIn
import java.net.{ InetSocketAddress, InetAddress }
import java.net.UnknownHostException
import java.io.{BufferedReader, InputStreamReader}
import java.net._

object TestClient {
  val port = 4000
  val packetSize = 512
  def main(args: Array[String]): Unit = {


    /**
      * Sends a msg on the UDP socket
      *
      * @param msg
      * @param ip address
      * @return response
      */
    def send(msg: String, ip: InetAddress):String = {
      val clientSocket = new DatagramSocket()
      val receiveData = new Array[Byte](packetSize)
      val sendData = msg.getBytes
      val sendPacket = new DatagramPacket(sendData, sendData.length, ip, port)
      clientSocket.send(sendPacket)
      val receivePacket = new DatagramPacket(receiveData, receiveData.length)
      clientSocket.receive(receivePacket)
      clientSocket.close();
      new String(receivePacket.getData())
    }

    val IPAddress = getInetAddress( if(args.length == 0) "localhost" else args(0))
    var run = true
    val testmessages = Array("GET /temperature",
                             "GET /potentiometer",
                             "PUT /LCD Wubba Lubba Dub Dub\\0",
                             "PUT /LED ORANGE",
                             "PUT /BUZZER 9-15",
                             "GET /BAD REQUEST"
                            )
    while(run){
      Console.println("Send to: " + IPAddress)
      Console.println("Test messages: ")
      for ( i <- 0 to (testmessages.length - 1)) {
         Console.println(i+": " + testmessages(i) )
      }
      Console.println("q: exit")
      StdIn.readLine() match{
        case "q" => run = false
        case str =>
          val message = testmessages(str.toInt)
          Console.println("Sending: " + message + " to " + IPAddress)
          val response = send(message, IPAddress)
          Console.println("Received: " + response)
      }
    }
  }

  /**
   * Gets the InetAddress from the ip, when the ip is not a hostaddress, use localhost
   * @param ip in String form
   * @return the converted InetAddress
   */
  private def getInetAddress(ip: String): InetAddress = {
    var ip_address: InetAddress = null

    try {
      ip_address = InetAddress.getByName(ip)

    } catch {
      case e: UnknownHostException =>
        println("The argument was not a valid ip address, ip chosen = localhost")
        ip_address = InetAddress.getByName("localhost")
    }

    return ip_address
  }
}
